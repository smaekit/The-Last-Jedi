//
//  ViewController.swift
//  The Last Jedi
//
//  Created by Marcus Jakobsson on 2017-11-16.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit
class ViewController: UIViewController {
    var character = Person(name: String(), height: String(), mass: String(), gender: String(), birth_year: String())

    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var genderLabel: UILabel!
    
    @IBOutlet weak var BirthLabel: UILabel!
    @IBOutlet weak var MassLabel: UILabel!
    @IBOutlet weak var HeightLabel: UILabel!
    @IBOutlet weak var characterNameLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        

    }
    
    override func viewWillAppear(_ animated: Bool) {
        characterNameLabel.text = character.name
        HeightLabel.text = character.height
        MassLabel.text = character.mass
        BirthLabel.text = character.birth_year
        genderLabel.text = character.gender
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

