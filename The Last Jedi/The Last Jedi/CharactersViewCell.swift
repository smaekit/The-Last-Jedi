//
//  CharactersViewCell.swift
//  The Last Jedi
//
//  Created by Hanna Rönnlund on 2017-11-16.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit

class CharactersViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
