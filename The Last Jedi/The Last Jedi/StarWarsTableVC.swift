//
//  StarWarsTableVC.swift
//  The Last Jedi
//
//  Created by Hanna Rönnlund on 2017-11-16.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit

class StarWarsTableVC: UITableViewController {

    var persons = [Person]()
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: "https://swapi.co/api/people/")
        var names = [String]()
        var birth_years = [String]()
        var heights = [String]()
        var massArray = [String]()
        var genders = [String]()
        

        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard error == nil else {
                print(error!)
                return
            }
            if let data = data {
                
                do {
                    if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        if let result = json["results"] as? [[String : Any]]
                        {
                            for object in result {
                                if let name = object["name"] as? String {
                                    print(name)
                                    names.append(name)
                                }
                                if let birth_year = object["birth_year"] as? String {
                                    print(birth_year)
                                    birth_years.append(birth_year)
                                }
                                if let height = object["height"] as? String {
                                    print(height)
                                    heights.append(height)
                                }
                                if let mass = object["mass"] as? String {
                                    print(mass)
                                    massArray.append(mass)
                                }
                                if let gender = object["gender"] as? String {
                                    print(gender)
                                    genders.append(gender)
                                }
                            }
                            
                            for i in 0..<names.count {
                                self.persons.append(Person(name: names[i], height: heights[i], mass: massArray[i], gender: genders[i], birth_year: birth_years[i]))
                            }
                            
                            
                            //Reloads tableview when all data is retrived from http request
                            DispatchQueue.main.async {
                                self.tableView.reloadData()
                            }
                            
                            
                            
                        }
                        
                        
                        
                    }
                    
                }catch{
                    print("Could not serialize")
                }
            }
            
        }
        
        task.resume()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return persons.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let cell = tableView.dequeueReusableCell(withIdentifier: "CharacterCell", for: indexPath) as! CharactersViewCell
        cell.nameLabel.text = self.persons[row].name
        print(self.persons[row].name)

        // Configure the cell...

        return cell
    }
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "characterInfo",
        let destination = segue.destination as? ViewController,
        let row = tableView.indexPathForSelectedRow?.row
        {
            destination.character = persons[row]
        }
    }


}
