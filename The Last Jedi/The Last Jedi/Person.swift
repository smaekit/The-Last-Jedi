//
//  Person.swift
//  The Last Jedi
//
//  Created by Hanna Rönnlund on 2017-11-16.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import Foundation

public class Person {
    var name : String
    var height : String
    var mass :String
    var gender : String
    var birth_year : String
    
    init(name : String, height : String, mass :String, gender : String, birth_year : String) {
        self.name = name
        self.height = height
        self.mass = mass
        self.birth_year = birth_year
        self.gender = gender
    }
}
